//
//  Car.swift
//  Classes and Objects
//
//  Created by mac on 9/30/18.
//  Copyright © 2018 Brahim Bouhouche. All rights reserved.
//

import Foundation

enum CarType {
    case Sedan
    case Coupe
    case Hatchback
}

class Car {
    var color : String
    var seatsNumber : Int
    var typeOfCar : CarType
    
    init(carColor : String,seats : Int, carType : CarType) {
        self.color = carColor
        self.seatsNumber = seats
        self.typeOfCar = carType
    }
}
