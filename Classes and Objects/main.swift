//
//  main.swift
//  Classes and Objects
//
//  Created by mac on 9/30/18.
//  Copyright © 2018 Brahim Bouhouche. All rights reserved.
//

import Foundation

print("Hello, World!")
let myCar = Car(carColor: "Black", seats: 8, carType: .Sedan)
let mySecCar = Car(carColor: "White", seats: 3, carType: .Hatchback)

print("\(myCar.color) which has \(myCar.seatsNumber) seats and it is \(myCar.typeOfCar)")
print("\(mySecCar.color) which has \(mySecCar.seatsNumber) seats and it is \(mySecCar.typeOfCar)")


